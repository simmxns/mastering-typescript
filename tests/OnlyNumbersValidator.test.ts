import OnlyNumbersValidator from '../src/validators/OnlyNumbersValidator'

describe('if is number return true', () => {
	test('truthines', () => {
		const onlyNumber = new OnlyNumbersValidator()

		expect(onlyNumber.isRegExpNumber('2020')).toBeTruthy()
		expect(onlyNumber.isRegExpNumber('000')).toBeTruthy()
		expect(onlyNumber.isRegExpNumber('-105')).toBeTruthy()
		expect(onlyNumber.isRegExpNumber(0x35)).toBeTruthy()
		expect(onlyNumber.isRegExpNumber(0o35)).toBeTruthy()
		expect(onlyNumber.isRegExpNumber('3.14')).toBeTruthy()
		expect(onlyNumber.isRegExpNumber('25.3333')).toBeTruthy()
		expect(onlyNumber.isRegExpNumber('-25.3333')).toBeTruthy()
		expect(onlyNumber.isRegExpNumber('.2529')).toBeTruthy()
		expect(onlyNumber.isRegExpNumber('-.29')).toBeTruthy()
		expect(onlyNumber.isRegExpNumber('abc')).not.toBeTruthy()
		expect(onlyNumber.isRegExpNumber('..123')).toBeFalsy()
		expect(onlyNumber.isRegExpNumber('3.')).toBeFalsy()
		expect(onlyNumber.isRegExpNumber('.12.3')).toBeFalsy()
	})
})
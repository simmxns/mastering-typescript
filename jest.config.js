const { pathsToModuleNameMapper } = require('ts-jest');
const { compilerOptions } = require('./tsconfig.json');

/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
	moduleNameMapper: pathsToModuleNameMapper(
		compilerOptions.paths, 
		{ prefix: '<rootDir>/' }
	),
  preset: 'ts-jest',
  testEnvironment: 'node',
  coverageDirectory: './coverage',
  testMatch: [ "**/?(*.)+(test).ts" ],
  testPathIgnorePatterns: [ 
    '/node_modules/', 
    '/dist/', 
    '/lib/' 
  ],
  resetMocks: true,
  clearMocks: true
}

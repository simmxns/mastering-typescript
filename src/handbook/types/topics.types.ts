import { Fact } from "./common.types"

/** Recieve a string key value and returns a Fact */
export type topicOption = {
	[key: string]: Fact
}

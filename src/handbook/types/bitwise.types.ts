export interface Bitwise {
	[key: string]: number
	OR: number
	XOR: number
	NOT: number
	LSHIFT: number
	RSHIFT: number
	NRSHIFT: number
	URSHIFT: number
	NURSHIFT: number
}
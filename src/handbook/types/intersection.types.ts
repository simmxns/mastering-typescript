/*
	You can merge two types in only one type with & (intersection types)
*/

export class User {
	name: string
}

export class Admin {
	permissions: number
	getPermissions() {}
}

// const user: User & Admin
// ^ const need to be initialized
let user!: User & Admin
user.name = "Simon"
user.permissions = 5
user.getPermissions()

// user = new User
// ^ Type 'User' is not assignable to type 'User & Admin'.
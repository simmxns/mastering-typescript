/**
 * Array.shift()
 * 
 * delete the first element of an array
 * it self returns the deleted element
 */

export const shift = ['29º', 'for', 'tomorrow']
shift.shift() // '29º'
shift // ['for', 'tomorrow']

/**
 * Array.unshift()
 * 
 * add an element at the start of an array
 * it self returns the added element
 */

export const unshift = shift.unshift('19º') // '19º'
unshift // ['19º', 'for', 'tomorrow']
/**
 * Array.filter()
 * 
 * Create new array if all elements meets a condition
 */

const filteredDB = [
	{
		name: 'Simon',
		username: '@username',
		face: '🤯'
	},
	{
		name: 'Sandra',
		username: '@username',
		face: '😝'
	}
]

function filterName(users: any): any {
	const dataFilter: string = 'Simon'
	if (dataFilter) {
		return users.name === dataFilter 
	}
	return users
}

filteredDB.filter(filterName) // // [ { name: 'Simon', username: '@username', face: '🤯' } ]

export const filter: Array<string> = ['🥳', '🦁', '🥳', '😜', '🥳']
filter.filter(value => value === '🥳') // ['🥳', '🥳', '🥳']

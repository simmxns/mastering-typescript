/**
 * Array.fill()
 * 
 * Change the value of the elements from a index
 * by default start value is 0 and end is this.length
 * returns the modified array
 */

export const fill = [1, 2, 3, 4, 5, 6]
fill.fill(6) 				// [ 6, 6, 6, 6, 6, 6 ]
fill.fill(0, 3) 		// [ 6, 6, 6, 0, 0, 0 ]
fill.fill(10, 4, 6) // [ 6, 6, 6, 0, 10, 10]
/**
 * Array.sort()
 * 
 * sort an array
 * by default the sort mode responds to the position of the string value according to its Unicode value.
 * (a, b) => a - b for asc order
 * (a, b) => b - a for desc order
 */

export const sort = [11, 20, 1, 5, 3, 2, 4, 9]

sort.sort() // [ 1, 11, 2, 20, 3, 4, 5, 9 ] ???
sort.sort((a, b) => a - b) // [ 1, 2, 3, 4, 5, 9, 11, 20 ]
sort.sort((a, b) => b - a) // [ 20, 11, 9, 5, 4, 3, 2, 1 ]
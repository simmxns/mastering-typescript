/**
 * Array.every()
 * 
 * Determines if all the elements into an array satisfy a condition
 * NOTE: empty array return 'true'
 * 
 */
export const every: Array<number> = [1, 20, 60, 203, 0o34, 44, 102, 11.5]

every.every(curr => curr > 0) // true 
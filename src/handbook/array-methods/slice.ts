/**
 * Array.slice()
 * 
 * Returns a copy of a part of the array
 * 
 * NOTE: negative values make that starts at the end
 */

export const slice = ['bye', 'hello', 'whats', 'up']

slice.slice(2) // ['whats', 'up']
slice.slice(0, 2) // ['bye', 'hello']
slice.slice(-1) // ['up']

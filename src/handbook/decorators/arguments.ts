/**
 * Decorator factory
 * 
 * Una funciton que retorna un decorador
 */
export function DecoratorFactory(msg: string): any {
	return function (target: any, propertyKey: string, descriptor?: any): any {
		const originalFunction = target[propertyKey]

		const decoratedFunction = function() {
			originalFunction()
			console.log(msg)
		}
		descriptor.value = decoratedFunction

		return descriptor
	}
}


class Speaker {
	@DecoratorFactory('oldN() deprecated use n() instead')
	oldN() { console.log(10) }

	@DecoratorFactory('n() executed...')
	n() { console.log(20) }
}

const speaker = new Speaker()
speaker.oldN()
speaker.n()
speaker.n()

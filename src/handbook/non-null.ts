import type { NonNullArray } from '@T/aliases.types'

/**
 * Non null assertion operator (!:)
 * 
 * This post-fix expression negates null and undefined types from variables
 * 
 * Basically, when a variable can become nullish or if this data type is not in its possibilities by putting the not null assertion operator, you allow the variable to be nullish
 *  
 * `--strictNullChecks`
 * 
 * `"strictNullChecks": false`
 * 
 * That params tell to typescript checker to ignore this feature
 * 
 * NOTE: This operator will only take effect when the strictNullChecks flag is on
 */
let x: number
let y: number | undefined
let z: number | undefined | null
x = 1
y = 1
z = 1
//x = undefined // Type 'undefined' is not assignable to type 'number'
y = undefined
z = undefined
//x = null			// Type 'null' is not assignable to type 'number'
//y = null 			// Type 'null' is not assignable to type 'number | undefined'
z = null
//x = y 				// Type 'undefined' is not assignable to type 'number'
//x = z 				// Type 'null' is not assignable to type 'number'
y = x
//y = z 				// Type 'null' is not assignable to type 'number | undefined'
z = x
z = y

export function nonNullNumber(a: number | undefined) {
	//const b: number = a // Type 'undefined' is not assignable to type 'number'
	const c: number = a!
}

export function nonNullString(maybeString: string | undefined | null) {
	//const onlyString: string = maybeString // Type 'undefined' is not assignable to type 'string'
	const ignoreUndefinedAndNull: string = maybeString!
}

type NumGenerator = () => number
export function ignoreNonNullFunc(numGenerator: NumGenerator | undefined) {
	//const num1 = numGenerator // Cannot invoke an object which is possibly 'undefined'
	const num2 = numGenerator!()
	return num2
}

const a: number | undefined = undefined
export const b: number = a!

/**
 * What about optional chaining?
 */
export const nonNullArray: NonNullArray = [
	{ value: 1 },
	{ value: 2 }
]
const item = nonNullArray.find(el => el.value === 2)
//const result = item.value + 1 // Object is possibly 'undefined'
//const resultOptional = item?.value + 1 // item?.value  is still possibly undefined
const resultNonNull = item!.value

/*export const ScrolldInput = () => {
	const ref = React.createRef<HTMLInputElement>()

	const goToInput = () => ref.current!.scrollIntoView() // Error -> ref.current is possibly null

	return (
		<div>
			<input ref={ref} />
			<button onClick={goToInput}>Go to input</button>
		</div>
	)
}*/

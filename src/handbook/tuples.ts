import { finiteTuple, infiniteTuple } from "./types/tuples.types";

/*
	Tuple changes in last versions of typescript
	you cannot put more values than those already declared
	inmutable list of diff values with specific order and finite length 
*/

export const __tuple1: finiteTuple = [ "bye", 21, false ]
// how can I get infinite tuple values again?
export const __tuple2: infiniteTuple = [ "hi", 22, true, ...[ "bye", 21, false, true, 'welcome' ] ]



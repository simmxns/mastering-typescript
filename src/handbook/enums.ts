/*
	Tomar conjunto de valores numericos y asignarles un nombre 
	que nos permita identificarlo de manera mas clara y expresiva 
	Son una conveniencia de expresividad y claridad al programa pero siguen siendo numeros
*/

export enum Sizes {
	sm = 1,
	md = 2,
	lg = 3,
}

/** 
 * typescript raise the value from 0 (by default) to ++ 
 * if the previous value is defined the next undefined value will be the follow number of the previous one
**/
enum PaymentState {
	created,
	paid,
	inDebt
}

declare class Shipping {
	size: number
}

const shipping: Shipping = new Shipping()

shipping.size = Sizes.sm
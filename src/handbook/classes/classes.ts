class Video {
  title: string;

  constructor (title: string) {
    this.title = title;
  }

  play(): void { console.log(`${this.title} is playing...`) }
  stop(): void { console.log("Video Stopped") }
}

class YoutubeVideo extends Video {
  constructor(title: string) {
    super(title) // Llama al constructor de la clase principal
    console.log("Initializing youtube")
  }

  play() {
    super.play() // Llama al metodo de la clase padre
    console.log("Playing a youtube video")
  }
}

export default Video

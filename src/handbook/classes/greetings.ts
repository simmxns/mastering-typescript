import ps from "prompt-sync"

export default class Greeter {
	private _name: string

	private requestName(): void {
		const prompt = ps()
		this._name = prompt("What's your name handsome?: ")
	}

	public greet(): string {
		return `Hello ${this._name}`
	}
	public formalGreet(): string {
		return `Greetings my lord ${this._name}`
	}
	public urbanGreet(): string {
		return `What's Popping ${this._name}`
	}
}

import { NumberValidator } from '@/handbook/interfaces/Validation'

export default class OnlyNumbersValidor implements NumberValidator {
	private regexValidator: RegExp = /^([0-9]||-)\d*(\.\d+)?$/

	public isRegExpNumber(s: string | number): boolean {
		if (typeof s === "string") {
			return s.match(this.regexValidator) !== null
		}
		return true
	}
}
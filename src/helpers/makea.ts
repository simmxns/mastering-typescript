import { joke, hardJoke, riddle } from '@/assets/content'
import { Fact } from '@/handbook/types/common.types'
import getQuote, { getAllQuotes } from '@/helpers/quotes'
import { topicOption } from '@/handbook/types/topics.types'

/** Generate a fact! Just pass the one that you need as a parameter<br>
 * Possible facts:
 * <ol>
 * <li>Quotes</li>
 * <li>All Quotes</li>
 * <li>Joke</li>
 * <li>HardJoke</li>
 * <li>Riddle</li>
 * </ol>
 */
export function makeA(topic: string): Fact {
	const topics: topicOption = {
		'quote': getQuote(),
		'all-quotes': getAllQuotes(),
		'joke': joke,
		'hardjoke': hardJoke,
		'riddle': riddle
	}
	const DEFAULT_TOPIC: string = "Selected option doesn't exist"
	return topics[topic.toLowerCase()] || DEFAULT_TOPIC
}

/** Calculator with basic operations */
export default class Calculator {
	constructor() {
		console.log("Calculator has been loaded success!")
	}

	/** Sum any quantity of values */
	sum(...values: number[]): number {
		return values.reduce((prev, curr) => prev + curr)
	}

	/** Substrac any quantity of values */
	diff(...values: number[]): number {
		return values.reduce((prev, curr) => prev - curr)
	}
}
/**
 * A generator function return diferent values in each call
 * It return a Generator object
 *
 * .next() go to the next yield and return it
 * .return() end the generator and return the given value
 * .throw() throw an error and end the generator
 */
export function* doStuff() {
	const emojis = ['😎', '🥰', '🤯']

	for (let i = 0; i < emojis.length; i++) {
		yield emojis[i] // stop the generation
	}
}

/** yield* is used to delegate to other Generator object */
export function* doMoreStuff() {
	const emojis = ['🤩', '🥺']

	for (let i = 0; i < emojis.length; i++) {
		yield emojis[i]
	}
	yield* doStuff()
}
/*
const ds = doStuff()
console.log(ds.next()) // { "value": "😎", "done": false }
console.log(ds.next()) // { "value": "🥰", "done": false }
console.log(ds.next()) // { "value": "🤯", "done": false }
*/
const dms = doMoreStuff()
console.log(dms.next(), dms.next(), dms.next(), dms.next(), dms.next())
/*
{
  "value": "🤩",
  "done": false
},  {
  "value": "🥺",
  "done": false
},  {
  "value": "😎",
  "done": false
},  {
  "value": "🥰",
  "done": false
},  {
  "value": "🤯",
  "done": false
}  
*/


import { isNullish } from '@/helpers/is-nullish'

export function tripleOccurrence(sentence: string) {
	const re: RegExp = /(.)\1\1+/g
	return isNullish(sentence.match(re)!) ? 0 : sentence.match(re)!.length
}
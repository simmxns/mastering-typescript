export interface DOGS_ACTIONS {
	type: 'GET_DOGS' | 'GET_DOG' | 'PUSH_LIKE'
	payload: any
}

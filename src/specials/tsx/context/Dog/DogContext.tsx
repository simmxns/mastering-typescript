import { createContext } from 'react'

export default createContext<{
	dogs: string[],
	liked: boolean
	getDogs?: () => Promise<void>
	giveLike?: () => void
}>({
	dogs: [],
	liked: false
})

import React, { useReducer, Reducer } from 'react'
import DogReducer from './DogReducer'
import DogContext from './DogContext'
import axios from 'axios'
import { DOGS_ACTIONS } from './types'

export default function DogState(props: any): JSX.Element {
	const initialState = {
		dogs: [],
		selected: null,
		liked: false
	}

	const [state, dispatch] = useReducer<Reducer<any, DOGS_ACTIONS>>(DogReducer, initialState)

	const getDogs: () => Promise<void> = async () => {
		const res = await axios.get('https://dog.ceo/api/breed/hound/images')
		dispatch({
			type: 'GET_DOGS',
			payload: res.data.message
		})
	}

	const getDog: (pos: number) => void = (pos: number) => {
		dispatch({
			type: 'GET_DOG',
			payload: state.dogs[pos]
		})
	}

	const giveLike: () => void = () => {
		dispatch({
			type: 'PUSH_LIKE',
			payload: !state.liked
		})
	}

	return (
		<DogContext.Provider
			value={{
				dogs: state.dogs,
				liked: state.liked,
				getDogs,
				giveLike
			}}
		>
			{props.children}
		</DogContext.Provider>
	)
}
